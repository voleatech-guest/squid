#! /usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND = -Wno-error=deprecated-declarations
export DEB_CXXFLAGS_MAINT_APPEND = -Wno-error=deprecated-declarations

ifneq (,$(filter $(DEB_HOST_ARCH), armel m68k mips mipsel powerpc powerpcspe sh4))
	DEB_LDFLAGS_MAINT_APPEND += -latomic
endif
export DEB_LDFLAGS_MAINT_APPEND

export DEB_BUILD_PARALLEL = yes
CXX_FOR_BUILD ?= $(CXX)

INSTALLDIR := $(CURDIR)/debian/tmp
datadir=/usr/share/squid

DEB_DH_INSTALL_SOURCEDIR := $(INSTALLDIR)
DEB_INSTALL_DOCS_squid-common := debian/copyright CONTRIBUTORS CREDITS QUICKSTART RELEASENOTES.html SPONSORS

BUILDINFO := $(shell lsb_release -si 2>/dev/null)

DEB_CONFIGURE_EXTRA_FLAGS := BUILDCXXFLAGS="$(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)" \
		BUILDCXX=$(CXX_FOR_BUILD) \
		--with-build-environment=default \
		--enable-build-info="$(BUILDINFO) $(DEB_HOST_ARCH_OS)" \
		--datadir=/usr/share/squid \
		--sysconfdir=/etc/squid \
		--libexecdir=/usr/lib/squid \
		--mandir=/usr/share/man \
		--enable-inline \
		--disable-arch-native \
		--enable-async-io=8 \
		--enable-storeio="ufs,aufs,diskd,rock" \
		--enable-removal-policies="lru,heap" \
		--enable-delay-pools \
		--enable-cache-digests \
		--enable-icap-client \
		--enable-follow-x-forwarded-for \
		--enable-auth-basic="DB,fake,getpwnam,LDAP,NCSA,PAM,POP3,RADIUS,SASL,SMB" \
		--enable-auth-digest="file,LDAP" \
		--enable-auth-negotiate="kerberos,wrapper" \
		--enable-auth-ntlm="fake,SMB_LM" \
		--enable-external-acl-helpers="file_userip,kerberos_ldap_group,LDAP_group,session,SQL_session,time_quota,unix_group,wbinfo_group" \
		--enable-security-cert-validators="fake" \
		--enable-storeid-rewrite-helpers="file" \
		--enable-url-rewrite-helpers="fake" \
		--enable-eui \
		--enable-esi \
		--enable-icmp \
		--enable-zph-qos \
		--enable-ecap \
		--disable-translation \
		--with-swapdir=/var/spool/squid \
		--with-logdir=/var/log/squid \
		--with-pidfile=/run/squid.pid \
		--with-filedescriptors=65536 \
		--with-large-files \
		--with-default-user=proxy

ifeq ($(DEB_HOST_ARCH_OS), kfreebsd)
		DEB_CONFIGURE_EXTRA_FLAGS += --enable-kqueue
endif
ifeq ($(DEB_HOST_ARCH_OS), linux)
		DEB_CONFIGURE_EXTRA_FLAGS += --enable-linux-netfilter --with-systemd
endif

DEB_MAKE_CLEAN_TARGET = distclean

%:
	dh $@

override_dh_auto_configure:
	mkdir -p debian/build-openssl
	# copy the source to build-openssl
	tar -cf - --exclude=debian/build* --exclude=.pc . \
		| tar -xf - -C debian/build-openssl
	# run buildconf and make sure to copy the patched ltmain.sh
	#for flavour in build build-gnutls build-nss; do \
	#	(cd debian/$$flavour && ./buildconf && cp ../../ltmain.sh .) \
	#done
	dh_auto_configure -- ${DEB_CONFIGURE_EXTRA_FLAGS} \
		--with-gnutls
	cd debian/build-openssl && dh_auto_configure -- ${DEB_CONFIGURE_EXTRA_FLAGS} \
		--with-openssl \
		--enable-ssl-crtd

override_dh_auto_build:
	dh_auto_build
	cd debian/build-openssl && dh_auto_build

override_dh_auto_test:
	-dh_auto_test
	-cd debian/build-openssl && dh_auto_test

override_dh_auto_install:
	dh_auto_install
	dh_auto_install --destdir=$(INSTALLDIR)-openssl -- -C debian/build-openssl

override_dh_installinit:
	dh_installinit -psquid
	dh_installinit -psquid-openssl --name=squid

execute_after_dh_auto_install:
	install -m 755 -g root -d $(INSTALLDIR)/usr/lib/cgi-bin
	install -m 755 -g root -d $(INSTALLDIR)-openssl/usr/lib/cgi-bin
	install -m 644 $(INSTALLDIR)/etc/squid/squid.conf.documented $(INSTALLDIR)-openssl/etc/squid/squid.conf
	install -m 644 $(INSTALLDIR)/etc/squid/squid.conf.documented $(INSTALLDIR)/etc/squid/squid.conf
	install -m 755 -g root -d $(INSTALLDIR)/etc/squid/conf.d
	install -m 755 -g root -d $(INSTALLDIR)-openssl/etc/squid/conf.d
	install -m 644 -g root debian/debian.conf $(INSTALLDIR)/etc/squid/conf.d/debian.conf
	install -m 644 -g root debian/debian.conf $(INSTALLDIR)-openssl/etc/squid/conf.d/debian.conf
	rm $(INSTALLDIR)-openssl/usr/lib/squid/cachemgr.cgi
	mv $(INSTALLDIR)/usr/lib/squid/cachemgr.cgi $(INSTALLDIR)/usr/lib/cgi-bin/cachemgr.cgi
	install -m 755 -g root -d $(INSTALLDIR)-openssl/etc/init.d
	install -m 755 -g root -d $(INSTALLDIR)-openssl/etc/logrotate.d
	install -m 755 -g root -d $(INSTALLDIR)-openssl/etc/resolvconf
	install -m 755 -g root -d $(INSTALLDIR)-openssl/etc/resolvconf/update-libc.d
	install -m 755 -g root -d $(INSTALLDIR)-openssl/etc/ufw/applications.d
	#install -m 755 -g root -d $(INSTALLDIR)/etc/init.d
	install -m 755 -g root -d $(INSTALLDIR)/etc/logrotate.d
	install -m 755 -g root -d $(INSTALLDIR)/etc/resolvconf
	install -m 755 -g root -d $(INSTALLDIR)/etc/resolvconf/update-libc.d
	install -m 755 -g root -d $(INSTALLDIR)/etc/ufw/applications.d
	install -m 755 -g root debian/squid.resolvconf $(INSTALLDIR)-openssl/etc/resolvconf/update-libc.d/squid
	install -m 644 -g root debian/squid.logrotate $(INSTALLDIR)-openssl/etc/logrotate.d/squid
	install -m 644 -g root debian/squid.ufw.profile $(INSTALLDIR)-openssl/etc/ufw/applications.d/squid
	install -m 755 -g root debian/squid.resolvconf $(INSTALLDIR)/etc/resolvconf/update-libc.d/squid
	install -m 644 -g root debian/squid.logrotate $(INSTALLDIR)/etc/logrotate.d/squid
	install -m 644 -g root debian/squid.ufw.profile $(INSTALLDIR)/etc/ufw/applications.d/squid
	install -m 755 -g root -d debian/squid-openssl/var/log
	install -m 755 -g root -d debian/squid-openssl/var/spool
	install -m 755 -g root -d debian/squid/var/log
	install -m 755 -g root -d debian/squid/var/spool
	install -m 750 -o proxy -g proxy -d debian/squid-openssl/var/log/squid
	install -m 750 -o proxy -g proxy -d debian/squid-openssl/var/spool/squid
	install -m 750 -o proxy -g proxy -d debian/squid/var/log/squid
	install -m 750 -o proxy -g proxy -d debian/squid/var/spool/squid
	install -m 755 -g root -d $(INSTALLDIR)-openssl/usr/share/man/man1
	install -m 755 -g root -d $(INSTALLDIR)/usr/share/man/man1
	rm $(INSTALLDIR)-openssl/usr/bin/purge
	rm $(INSTALLDIR)-openssl/usr/share/man/man1/purge.1
	mv $(INSTALLDIR)/usr/bin/purge $(INSTALLDIR)/usr/bin/squid-purge
	mv $(INSTALLDIR)/usr/share/man/man1/purge.1 $(INSTALLDIR)/usr/share/man/man1/squid-purge.1
	install -m 755 -g root -d $(INSTALLDIR)-openssl/etc/apparmor.d/force-complain
	install -m 755 -g root -d $(INSTALLDIR)-openssl/etc/apparmor.d/disable
	install -m 755 -g root -d $(INSTALLDIR)/etc/apparmor.d/force-complain
	install -m 755 -g root -d $(INSTALLDIR)/etc/apparmor.d/disable
	install -m 644 -g root debian/usr.sbin.squid $(INSTALLDIR)-openssl/etc/apparmor.d
	install -m 644 -g root debian/usr.sbin.squid $(INSTALLDIR)/etc/apparmor.d
	dh_apparmor --profile-name=usr.sbin.squid -psquid-openssl
	dh_apparmor --profile-name=usr.sbin.squid -psquid

override_dh_install:
	dh_install -psquid -psquid-common -psquidclient -psquid-cgi -psquid-purge \
		--sourcedir=$(INSTALLDIR)
	dh_install -psquid-openssl \
		--sourcedir=$(INSTALLDIR)-openssl

override_dh_installsystemd:
	dh_installsystemd -psquid
	dh_installsystemd -psquid-openssl --name=squid

override_dh_auto_clean:
	$(RM) -r debian/build* debian/tmp*
	dh_auto_clean

# Disable dh_missing
override_dh_missing:

debian/copyright: debian/copyright.Debian CREDITS
	cat $^ | sed -e "s/59 Temple Place.* Suite 330/51 Franklin St, Fifth Floor/" -e "s/MA  \?02111[^, ]*/MA 02110-1301/" -e "s/675 Mass Ave/51 Franklin St, Fifth Floor/" -e "s/Cambridge, MA 02139/Boston, MA 02110-1301/" > $@
